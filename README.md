# Nuit de l'Info 2022

# Description

Un site web de visualisation de donnée en rapport au sujet principal de la nuit de l'info.
L'aspect simpliste et ecologique du site est une priorité.

# Défis choisis

- https://www.nuitdelinfo.com/inscription/defis/354
- https://www.nuitdelinfo.com/inscription/defis/359
- https://www.nuitdelinfo.com/inscription/defis/358
- https://www.nuitdelinfo.com/inscription/defis/336
- https://www.nuitdelinfo.com/inscription/defis/357

# Historique

- [x] ajout de l'addon GreenIT
- [ ] 

# Contact

Linkedin
Gitlab 