drop database if exists nuit_info_2022;
create database nuit_info_2022;
use nuit_info_2022;

# création des tables liées au virus
create table Virus (
	id integer,
	name varchar(20)
);

create table Symptome (
	id integer
);

create table Prevention (
	id integer
);

create table Traitement (
	id integer
);

# création des tables liées au lieux
create table Marqueur (
	id integer,
	gps_x integer,
	gps_y integer
);

create table Ville (
	id integer,
	name varchar(20)
);

create table Distributeur (
	id integer
);

create table CentreDepistage (
	id integer
);


create table Quiz (
	id integer,
	question varchar("255"),
	reponse1 varchar("255"),
	reponse2 varchar("255"),
	reponse3 varchar("255"),
	reponse4 varchar("255"),
	solution integer
);