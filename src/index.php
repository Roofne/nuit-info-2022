<?php
// Chargement du modèle selon la page
require_once("./model/Virus.php");

// Chargement des données depuis la BDD
function connect() {
    $servername = "mysql-roofne.alwaysdata.net";
    $dbname     = "roofne_nuit_info_2022";
    $username   = "roofne";
    $password   = "TPLa9c51999";

    $db = new PDO('mysql:host='.$servername.';dbname='.$dbname.'', $username, $password);
    return $db;
}

function get_all_virus($db) {
    $sql = "select * from Virus";
    $request = $db->query($sql);
    $rows = $request->fetchAll();
    return $rows;
}

function get_all_symptome($db) {
    $sql = "select * from Symptome";
    $request = $db->query($sql);
    $rows = $request->fetchAll();
    return $rows;
}

function get_all_prevention($db) {
    $sql = "select * from Prevention";
    $request = $db->query($sql);
    $rows = $request->fetchAll();
    return $rows;
}

function get_all_traitement($db) {
    $sql = "select * from Traitement";
    $request = $db->query($sql);
    $rows = $request->fetchAll();
    return $rows;
}

// Rendu des pages
print("
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <title>Nuit Info 2022</title>
    <link rel='stylesheet' href='./static/css/main.css'>
    <link rel='stylesheet' type='text/css' href='./static/css/main.css' media='print'>
</head>
<body>
");
require_once("./back/base/header.php");

print("<nav>");
print("<div class='nav-item'><h2>IST</h2>");
foreach(get_all_virus(connect()) as $row) {
    print("<div class='sub-bar-item'><a href='#".$row["id"]."'>".$row["name"]."</a></div>");
}
print("</div>");
print("<div class='nav-item'><h2>Symptome</h2>");
foreach(get_all_symptome(connect()) as $row) {
    print("<div class='sub-bar-item'><a href='#".$row["id"]."'>".$row["name"]."</a></div>");
}
print("</div>");
print("<div class='nav-item'><h2>Prevention</h2>");
foreach(get_all_prevention(connect()) as $row) {
    print("<div class='sub-bar-item'><a href='#".$row["id"]."'>".$row["name"]."</a></div>");
}
print("</div>");
print("<div class='nav-item'><h2>Traitement</h2>");
foreach(get_all_traitement(connect()) as $row) {
    print("<div class='sub-bar-item'><a href='#".$row["id"]."'>".$row["name"]."</a></div>");
}
print("</div>");
print("<div class='nav-item'><h2>Quiz</h2></div>");
print("</nav>");


print("<div><table>");
foreach(get_all_virus(connect()) as $row) {
    $virus = new Virus($row['id'], $row['name']); 
    print("<tr><td>".$virus->id."</td><td>".$virus->name."</td></tr>");
}
print("<table></div>");

require_once("./back/base/footer.php");
require_once("./back/base/html_end.php");
?>