<?php
class Position {
    // Définition des attributs
    public $gps_x;
    public $gps_y;

    // Définition des constructeurs
    public function __construct($_x, $_y) {
        $this->gps_x = $_x;
        $this->gps_y = $_y;
    }
}
?>