<?php
class Virus {
    // définition des attributs
    public $id;
    public $name;
  
    // Définitions des constructeurs
    public function __construct($_id, $_name) {
      $this->id = $_id;
      $this->name = $_name;
    }

    // Définition des getters et setters
    public function getID() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }
  
    // Définitions des méthodes
    public function toString() {
        return strval($this->id)." | ".$this->name;
    }

    public function get_symptome($db) {
        return "?";
    }

    public function get_contraception($db) {
        return "?";
    }

    public function get_traitement($db) {
        return "?";
    }
  } 
?>